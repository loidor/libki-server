Det här är en Windowsport av Libki, på svenska.

Manual
******

Det finns tre användarnivåer. User är biblioteksbesökare. Admin är våra arbetsdatorer och ger inlogg till administrationsgränssnittet. 
Super-Admin är samma som föregående, fast innehåller dessutom fliken Inställningar.
    
Inställningarna är mycket självförklarande, och innehåller hur mycket tid varje användare får som default per dag, 
hur mycket tid varje användare får per inloggning (Det går att sätta t ex 120 minuter per dag, men bara 60 minuter per inloggning),
timeout på hur lång tid en klient får anses som online om den inte kommunicerat med servern, huruvida datorerna är öppna eller om reservation
krävs, eller en kombination däremellan samt reservationens timeout (hur lång tid användaren har på sig att logga in innan reservationen tas bort).

Det finns också inställningar för att generera flera gästpass, för att ha banners på klientdatorerna samt för att länka användaren till 
bibliotekssystemet (fungerar exempelvis med Koha).
    

Ändringar
*********
1.  Systemet har testats i Windowsmiljö. För att allt ska funka har två värden i tabellen users fått sina NOT NULL borttagna.
1.1 Perl installeras med Strawberry perl snarare än Activestate eftersom Strawberry är open source.
1.2 MySQL behöver installeras via Microsoft Web Platform Installer (wpilauncher.exe).
1.3 Alla CPAN-moduler behöver installeras med cpanm och några behöver installeras med --force då deras tester inte körs klart ordentligt när de byggs. 
    Net::Server behöver dessutom installeras med --notest eftersom ett test fastnar. Alla moduler fungerar fint trots --force och --notest.
1.4 Ett scheduler.bat-script har skrivits för att automatisera inläggningarna i Schemaläggaren. 

2.   Översättningar. Admin-delen har översatts i två steg. Dels har all html (*.tt-filer som ligger i root/dynamic) översatts, 
     och dels har ett jQuery-script lagts in i wrapper.tt som översätter vissa ord som genereras av olika inlästa jQuery-script.

3.   När det gäller stängningstider kollar closinghours.pl när biblioteket stänger för dagen mot en tabell, closinghours, 
    och om differensen mellan nu och stängning är mindre än defaultinställningen minskas den aktiva användarens minuter till rätt antal minuter till stängning om
    användaren för närvarande har fler minuter än tid till stängning. Inställning av detta görs under Stängningstider i administrationsdelen.

Installationshänvisning
***********************

Source ligger på https://bitbucket.org/loidor/libki-server/src.

Övriga program som behövs under installationen ligger på https://bitbucket.org/loidor/libki-server/downloads.

1.  kopiera över filerna i source till C:\libki-server. 

2.  Starta wpilauncher.exe

3.  Välj Products-fliken och sök efter MySQL. Installera MySQL Windows 5.5 (eller högre). 

4.  installera Strawberry Perl med MSI-paketet som följer med.

5.  Skapa ny MySQL-användare och skapa en tom databas i cmd eller shell. 
        C:\>    mysql -u root -p
        mysql>  CREATE USER 'libkiuser'@'localhost' IDENTIFIED BY 'libkipassword';
        mysql>  GRANT ALL PRIVILEGES ON libki.* TO 'libkiuser'@'localhost';
        mysql>  CREATE DATABASE libki;
        mysql>  FLUSH PRIVILEGES;
        mysql>  EXIT;

6.  uppdatera libki-server\libki_local.conf.example med samma data (libkiuser, libkipassword) och spara som libki-server\libki_local.conf.

7.  installera alla dependencies via shell eller cmd
7.1     C:\>    cpan install App::cpanminus
7.2     C:\>    cpanm Net::Server --notest
7.3     C:\>    cpanm Catalyst::Engine::HTTP::Prefork --force
7.4     C:\>    cpanm DBIx::Class::TimeStamp
7.5     C:\>    cpanm DBIx::Class::EncodedColumn
7.6     C:\>    cpanm DBIx::Class::Numeric
7.7     C:\>    cpanm DBIx::Class::Cursor::Cached  
7.8     C:\>    cpanm DateTime::Format::DateParse
7.9     C:\>    cpanm Catalyst::Devel --force
7.10    C:\>    cpanm Catalyst::Plugin::StatusMessage
7.11    C:\>    cpanm Catalyst::Plugin::Session::State::Cookie
7.12    C:\>    cpanm Catalyst::Plugin::Session::Store::File
7.13    C:\>    cpanm Catalyst::Plugin::Authorization::Roles
7.14    C:\>    cpanm Catalyst::Plugin::StackTrace
7.15    C:\>    cpanm Catalyst::Authentication::Realm::SimpleDB
7.16    C:\>    cpanm Catalyst::View::JSON
7.17    C:\>    cpanm Catalyst::View::TT
7.18    C:\>    cpanm Perl6::Junction
7.19    C:\>    cpanm Config::JFDI
7.20    C:\>    cpanm SQL::Script
7.21    C:\>    cpanm File::Slurp
7.22    C:\>    cpanm String::Random
7.23    C:\>    cpanm MooseX::NonMoose

8.  Kör "libki-server\installer\update_db.pl" för att fylla databasen med allt som behövs.

9.  Kör "libki-server\script\administration\create_user.pl -u *ANVÄNDARNAMN* -p *LÖSENORD* -s -m 999" för att skapa en superadmin. 

10. Kör scheduler.bat för att lägga till timer, nightly för städning samt uppstart av servern och stängningstider vid omstart i schemaläggaren. 
        Det lägger till fyra batchscript som startar script\libki_server.pl samt script\cronjobs\closinghours.pl att köras vid varje uppstart, script\cronjobs\libki.pl att köras varje minut
        samt script\cronjobs\libki_nightly.pl att köras vid midnatt. Det första startar servern (port kan anges med argumentet -p, exempelvis libki_server.pl -p 1234). 
        Det andra sköter stängningstider utifrån tabellen closinghours. Det tredje sköter timern som minskar användarnas minuter varje minut 
        och det fjärde städar systemet vid midnatt, tar bort skapade gästkonton och liknande.
        
11. KLART! Bokningssystemet hittar du på http://127.0.0.1:3000 och administrationssystemet finns på http://127.0.0.1:3000/administration.